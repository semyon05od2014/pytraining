import sys

# считывание списка из входного потока
s = sys.stdin.readlines()
lst_in = [list(map(int, x.strip().split())) for x in s]

# здесь продолжайте программу (используйте список lst_in)
ans = []

for i in range(len(lst_in)):
    for j in range(i + 1, len(lst_in)):
        if lst_in[i][j] == lst_in[j][i]:
            ans.append('ДА')
        else:
            ans.append('НЕТ')

if 'НЕТ' in ans:
    print('НЕТ')
else:
    print('ДА')
