"""Подвиг 3. Вводится натуральное число n.
Необходимо найти все простые числа, которые меньше этого числа n, то есть,
в диапазоне [2; n). Результат вывести на экран в строчку через пробел.

Sample Input:

11
Sample Output:

2 3 5 7
"""
x = 11

# x = int(input())
# проверяем число на натуральность
def is_prime(a):
    if a < 2:
        return False
    for i in range(2, int(a ** 0.5 + 1)):
        if a % i == 0:
            return False
    else:
        return True
# print(is_prime(int(input())))


# Пробегаем проверкой по каждому числу последовательности

for j in range(x):
    if is_prime(j) == True:
        print(j, end=' ')


# is_prime(x)

"""
n = int(input())
for i in range(2, n) :
    for j in range(2, i):
        if i % j == 0 :
                break
    else :
        print(i, end=' ')
"""
